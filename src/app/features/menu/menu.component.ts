import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../../services/heroes.service';
import { Heroe } from '../../classes/heroe';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  public heroes: Heroe[];
  public hero: Heroe;
  public searchText: any;
  public filteredArray = [];

  constructor(private heroesService: HeroesService) { }

  ngOnInit(): void {
    this.heroesService.getAllHeroes().then((data: Heroe[]) => {
      this.heroes = data.map(value => new Heroe(value));
      this.filteredArray = data.map(value => new Heroe(value));
    });
    this.oneHero();
  }

  public oneHero() {
      this.heroesService.getHeroeById(1).then((data: Heroe) => {
        this.hero = data;
      });
  }

  public filterArray(name: string) {
    let emptyArray = [];
    name = name.toLowerCase();
    for(let hero of this.heroes) {
      let value = hero.nombre.toLowerCase();
      if(value.indexOf(name) >= 0) {
        emptyArray.push(hero)
      }
    }
    this.filteredArray = emptyArray.map(value => new Heroe(value));
  }
}
