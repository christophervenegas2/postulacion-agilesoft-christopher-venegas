import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Heroe } from '../classes/heroe';
import { stringify } from 'querystring';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {
  public uri = 'http://157.245.138.232:9091/api/v1/test/superheroes';

  constructor(private http: HttpClient) { }

  public async getAllHeroes(): Promise<Heroe[]> {
    return new Promise<Heroe[]>((resolve, reject) => {
      this.http.get(`${this.uri}`).subscribe((data: any) => {
        resolve(data.data.map(value =>  new Heroe(value)));
      });
    });
  }

  public async getHeroeById(id: number): Promise<Heroe> {
    return new Promise<Heroe>((resolve, reject) => {
      this.http.get(`${this.uri}/${id}`).subscribe((data: any) => {
        resolve(new Heroe(data.data));
      });
    });
  }

  private getHeaders() {
    const headers = new HttpHeaders(
      {
        'Access-Control-Allow-Origin' : '*',
        'Content-Type': 'application/json'
      }
    );
    return headers;
  }
}
