export class Heroe {
  public id: number;
  public nombre: string;
  public nombreReal: string;
  public puedeVolar: boolean;
  public avatarURL: string;

  constructor (data: any = null) {
    if (data) {
      this.id = data.id;
      this.nombre = data.nombre;
      this.nombreReal = data.nombreReal;
      this.puedeVolar = data.puedeVolar;
      this.avatarURL = data.avatarURL;
    }
  }
}
